# Code structure
The code is inside the `code/<chapter_number-what_is_it_about>/src` path.
Each of these "src" folders have 2 files:
- `pure.py` Pure python without state machines.
- `state_machine.py` State machine implementation for the same code.

# Running the code

*You can use virtualenv to isolate your python environments.*

- Install python3 (your machine may already have it!).
- `pip install -r requirements.txt`
- cd into the folder you want to browse.  
    `cd code/whatever_you_want_to_test/src` 
- Run the code
    `python <filename>.py`
- 👍
