import random
from functools import wraps

from states import States
from logger import logger


def log_state(fn):
  @wraps(fn)
  def inner(self, *args, **kwargs):
    logger.debug(f"IN_STATE: {self.state}")
    return fn(self, *args, **kwargs)

  return inner


class Order:
  def __init__(self):
    super(Order, self).__init__()
    self.state = States.CREATED

  def _handle_state_error(self):
    raise RuntimeError(f"INVALID_STATE | state: {self.state}")

  @log_state
  def start(self):
    if self.state in [States.PACKING_ITEMS, States.COMPLETED,
                      States.CHECKING_ITEMS, States.CANCELLED]:
      self._handle_state_error()

    self.state = States.PACKING_ITEMS
    logger.info("Packing items...")

  @log_state
  def abort(self):
    if self.state not in [States.CREATED]:  # whitelisted for simplicity
      self._handle_state_error()

    self.on_enter_CANCELLED()

  @log_state
  def check(self):
    if self.state in [States.CREATED, States.COMPLETED,
                      States.CHECKING_ITEMS, States.CANCELLED]:
      self._handle_state_error()

    self.is_packing_over() and self.all_items_in_bags()  # new here

    self.state = States.CHECKING_ITEMS
    logger.info("Checking items...")

  @log_state
  def cancel(self):
    if self.state in [States.CREATED, States.COMPLETED, States.CANCELLED]:
      self._handle_state_error()

    self.on_enter_CANCELLED()  # common to cancelled state

    self.handle_picked_items()  # specific to function

  @log_state
  def complete(self):
    if self.state not in [States.CHECKING_ITEMS]:  # Added newly
      self._handle_state_error()

    self.get_delivery()  # new here

    self.is_delivery_available()  # new here

    self.state = States.COMPLETED
    logger.info("Assigning to Delivery Executives...")

  def handle_picked_items(self):
    logger.info('Putting back picked items...')

  def on_enter_CANCELLED(self):
    self.state = States.CANCELLED
    logger.info("Sunken cost...")

  def is_packing_over(self):
    v = random.choice([True, False])
    if not v:  # hack to demonstrate. Not ideal.
      raise RuntimeError("STILL_PACKING")
    return True

  def is_checking_over(self):
    v = random.choice([True, False])
    if not v:  # hack to demonstrate. Not ideal.
      raise RuntimeError("STILL_CHECKING")
    return True

  def all_items_in_bags(self):
    v = random.choice([True, False])
    if not v:  # hack to demonstrate. Not ideal.
      raise RuntimeError("ITEMS_NOT_IN_BAGS")
    return True

  def is_delivery_available(self):
    if self.delivery:
      logger.info(f"FOUND_DELIVERY_WITH_ID: {self.delivery['id']}")
      return True
    raise RuntimeError("NO_DELIVERY_FOUND")

  def get_delivery(self):
    self.delivery = {'id': 1} if random.choice([True, False]) else None


if __name__ == '__main__':
  logger.info("USING PURE PYTHON\n")

  order = Order()

  order.start()

  order.check()

  # order.complete()
  # logger.info(f"4 --- {order.state}\n")
