import random

from transitions import Machine

from logger import logger
from states import States


class Order(Machine):
  def __init__(self):
    super(Order, self).__init__(states=States,
                                initial=States.CREATED,
                                before_state_change='log_state',
                                transitions=[
                                  {'trigger': 'start', "source": States.CREATED, "dest": States.PACKING_ITEMS,
                                   'after': 'pack_items'},

                                  {'trigger': 'check', "source": States.PACKING_ITEMS, "dest": States.CHECKING_ITEMS,
                                   'after': 'check_items',
                                   'conditions': ['is_packing_over', 'all_items_in_bags']  # this is new
                                   },

                                  {'trigger': 'complete', "source": States.CHECKING_ITEMS,
                                   'prepare': 'get_delivery',  # new funda
                                   'conditions': 'is_delivery_available',
                                   "dest": States.COMPLETED, 'after': 'assign_delivery'},

                                  {'trigger': 'abort', "source": States.CREATED, "dest": States.CANCELLED},

                                  {'trigger': 'cancel',
                                   "source": [States.PACKING_ITEMS, States.CHECKING_ITEMS],
                                   'dest': States.CANCELLED, 'after': 'handle_picked_items'}])

  def log_state(self):
    logger.debug(f"IN_STATE: {self.state}")

  # prepare
  def get_delivery(self):
    self.delivery = {'id': 1} if random.choice([True, False]) else None

  # conditions
  def is_packing_over(self):
    v = random.choice([True, False])
    if not v:  # hack to demonstrate. Not ideal.
      raise RuntimeError("STILL_PACKING")
    return True

  def all_items_in_bags(self):
    v = random.choice([True, False])
    if not v:  # hack to demonstrate. Not ideal.
      raise RuntimeError("ITEMS_NOT_IN_BAGS")
    return True

  def is_delivery_available(self):
    if self.delivery:
      logger.info(f"FOUND_DELIVERY_WITH_ID: {self.delivery['id']}")
      return True
    raise RuntimeError("NO_DELIVERY_FOUND")

  # State transition callbacks
  def on_enter_CANCELLED(self):
    logger.info("Sunken cost...")

  # `after` callbacks
  def pack_items(self):
    logger.info("Packing items...")

  def check_items(self):
    logger.info("Checking items...")

  def assign_delivery(self):
    logger.info("Assigning to Delivery Executives...")

  def handle_picked_items(self):
    logger.info('Putting back picked items...')


if __name__ == '__main__':
  logger.info("USING STATE MACHINES\n")

  order = Order()

  order.start()

  order.check()

  order.complete()  # should throw error
