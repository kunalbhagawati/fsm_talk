import logging

logger = logging.getLogger('root')
logger.addHandler(logging.StreamHandler())
logger.setLevel(logging.DEBUG)
