from states import States


class Order:
  def __init__(self):
    self.state = States.CREATED

  def start(self):
    if self.state in [States.IN_PROGRESS, States.COMPLETED]:
      raise RuntimeError(f"INVALID_STATE | state: {self.state}")

    self.state = States.IN_PROGRESS

  def complete(self):
    if self.state in [States.CREATED, States.COMPLETED]:
      raise RuntimeError(f"INVALID_STATE | state: {self.state}")

    self.state = States.COMPLETED


if __name__ == '__main__':
  print("USING PURE PYTHON\n")

  order = Order()
  print(f"1 --- {order.state}\n")

  order.start()
  print(f"2 --- {order.state}\n")

  order.start() # Should throw error

  order.complete()
  print(f"3 --- {order.state}\n")

  order.complete()
