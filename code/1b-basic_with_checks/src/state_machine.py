from transitions import Machine

from states import States


class Order(Machine):
  def __init__(self):
    super(Order, self).__init__(states=States,
                                initial=States.CREATED,
                                transitions=[{'trigger': 'start', "source": States.CREATED, "dest": States.IN_PROGRESS},
                                             {'trigger': 'complete', "source": States.IN_PROGRESS,
                                              "dest": States.COMPLETED}])


if __name__ == '__main__':
  print("USING STATE MACHINES\n")

  order = Order()
  print(f"1 --- {order.state}\n")

  order.start()
  print(f"2 --- {order.state}\n")

  order.start() # Should throw error

  order.complete()
  print(f"3 --- {order.state}\n")

  order.complete() # Should throw error
