from transitions import Machine

from states import States


class Order(Machine):
  def __init__(self):
    super(Order, self).__init__(states=States,
                                initial=States.CREATED,
                                transitions=[
                                  {'trigger': 'start', "source": States.CREATED, "dest": States.PACKING_ITEMS,
                                   'after': 'pack_items'},

                                  # new transition
                                  {'trigger': 'check', "source": States.PACKING_ITEMS, "dest": States.CHECKING_ITEMS,
                                   'after': 'check_items'},

                                  {'trigger': 'complete', "source": States.CHECKING_ITEMS,
                                   "dest": States.COMPLETED, 'after': 'assign_delivery'},

                                  # new transitions
                                  {'trigger': 'abort', "source": States.CREATED, "dest": States.CANCELLED},

                                  {'trigger': 'cancel',
                                   "source": [States.PACKING_ITEMS, States.CHECKING_ITEMS],
                                   'dest': States.CANCELLED, 'after': 'handle_picked_items'}])

  # State transition callbacks
  def on_enter_CANCELLED(self):
    print("Sunken cost...")

  # `after` callbacks
  def pack_items(self):
    print("Packing items...")

  def check_items(self):
    print("Checking items...")

  def assign_delivery(self):
    print("Assigning to Delivery Executives...")

  def handle_picked_items(self):
    print('Putting back picked items...')


if __name__ == '__main__':
  print("USING STATE MACHINES\n")

  order = Order()
  print(f"1 --- {order.state}\n")

  order.start()
  print(f"3 --- {order.state}\n")

  order.cancel()
  print(f"4 --- {order.state}\n")

  # order.complete()  # should throw error
