from states import States


class Order:
  def __init__(self):
    super(Order, self).__init__()
    self.state = States.CREATED

  def _handle_state_error(self):
    raise RuntimeError(f"INVALID_STATE | state: {self.state}")

  def start(self):
    if self.state in [States.PACKING_ITEMS, States.COMPLETED,
                      States.CHECKING_ITEMS, States.CANCELLED]:
      self._handle_state_error()

    self.state = States.PACKING_ITEMS
    print("Packing items...")

  def abort(self):
    if self.state not in [States.CREATED]:  # whitelisted for simplicity
      self._handle_state_error()

    self.on_enter_CANCELLED()

  def check(self):
    if self.state in [States.CREATED, States.COMPLETED,
                      States.CHECKING_ITEMS, States.CANCELLED]:
      self._handle_state_error()

    self.state = States.CHECKING_ITEMS
    print("Checking items...")

  def cancel(self):
    if self.state in [States.CREATED, States.COMPLETED, States.CANCELLED]:
      self._handle_state_error()

    self.on_enter_CANCELLED()  # common to cancelled state

    self.handle_picked_items()  # specific to function

  def complete(self):
    if self.state not in [States.CHECKING_ITEMS]:  # Added newly
      self._handle_state_error()

    self.state = States.COMPLETED
    print("Assigning to Delivery Executives...")

  def handle_picked_items(self):
    print('Putting back picked items...')

  def on_enter_CANCELLED(self):
    self.state = States.CANCELLED
    print("Sunken cost...")


if __name__ == '__main__':
  print("USING PURE PYTHON\n")

  order = Order()
  print(f"1 --- {order.state}\n")

  order.start()
  print(f"2 --- {order.state}\n")

  order.cancel()
  print(f"3 --- {order.state}\n")

  # order.complete()
  # print(f"4 --- {order.state}\n")
