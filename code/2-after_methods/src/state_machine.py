from transitions import Machine

from states import States


class Order(Machine):
  def __init__(self):
    super(Order, self).__init__(states=States,
                                initial=States.CREATED,
                                transitions=[{'trigger': 'start', "source": States.CREATED, "dest": States.IN_PROGRESS,
                                              'after': 'pack_items'},
                                             {'trigger': 'complete', "source": States.IN_PROGRESS,
                                              "dest": States.COMPLETED, 'after': 'assign_delivery'}])

  def pack_items(self):
    print("Packing items...")

  def assign_delivery(self):
    print("Assigning to Delivery Executives...")


if __name__ == '__main__':
  print("USING STATE MACHINES\n")

  order = Order()
  print(f"1 --- {order.state}\n")

  order.start()
  print(f"2 --- {order.state}\n")

  order.complete()
  print(f"3 --- {order.state}\n")
