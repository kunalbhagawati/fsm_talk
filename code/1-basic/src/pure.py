from states import States


class Order:
  def __init__(self):
    self.state = States.CREATED

  def start(self):
    self.state = States.IN_PROGRESS

  def complete(self):
    self.state = States.COMPLETED


if __name__ == '__main__':
  print("USING PURE PYTHON\n")

  order = Order()
  print(f"1 --- {order.state}\n")

  order.start()
  print(f"2 --- {order.state}\n")

  order.complete()
  print(f"3 --- {order.state}\n")
