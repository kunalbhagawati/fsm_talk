from enum import Enum


class States(str, Enum):
  CREATED = 'CREATED'
  IN_PROGRESS = 'IN_PROGRESS'
  COMPLETED = 'COMPLETED'
